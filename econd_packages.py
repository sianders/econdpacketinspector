from construct import Array, Bytes, BitStruct, If, RawCopy, Struct, Const, BitsInteger, Checksum, len_, this, Flag, FlagsEnum, Adapter, IfThenElse, setGlobalPrintFalseFlags, Enum, Bit, Bitwise, Bytewise
from construct.core import evaluate
from bitstring import BitArray
from hgcal_crc import calculate_econd_header_crc, calculate_econd_payload_crc

ECON_ERX = 6

setGlobalPrintFalseFlags(enabled=True)

RECONSTCTION_MODE = {
    "Perfect reconstruction": 0b00,
    "Good reconstruction": 0b01,
    "Failed reconstruction": 0b10,
    " Ambiguous reconstruction": 0b11
}

Header = Struct(
    'header_body' / RawCopy(BitStruct(
        'header_marker' / Const(0x154, BitsInteger(9)),
        'payload_length' / BitsInteger(9),
        'pass_through' / Flag, # 0 = standard, 1 = pass through  # type: ignore
        'expected' / Flag, # type: ignore
        'header_trailer_reconstruction' / Enum(BitsInteger(2), **RECONSTCTION_MODE),
        'event_bx_orbit_reconstruction' / Enum(BitsInteger(2), **RECONSTCTION_MODE),
        'match' / Flag, # type: ignore
        'truncated' / Flag, # type: ignore
        'hamming' / BitsInteger(6),
        'bx_number' / BitsInteger(12),
        'l1a_number' / BitsInteger(6),
        'orbit_number' / BitsInteger(3),
        'stat_or' / Flag, # type: ignore
        'reset_request' / BitsInteger(2),
    )),
    'crc' / Checksum(Bytes(1),
                     lambda data: calculate_econd_header_crc(data),
                     this.header_body.data),
)

class ChannelMap(Adapter):
    """Convert the channel map to a address list.

    """
    def __init__(self, subcon):
        super().__init__(subcon)

    def _decode(self, obj, context, path):
        s = slice(2, None) # Remove the 0b prefix
        return [i for i, c in enumerate(bin(obj)[s]) if c == '1']

    def _encode(self, obj, context, path):
        s = ['0'] * self.subcon.length # type: ignore
        for idx in obj:
            s[idx] = '1'
        return int(''.join(s), 2)

SubpacketHeader = BitStruct(
    'stat' / FlagsEnum(BitsInteger(3), HT_match=0b100, EBO_match=0b010, ROC_CRC=0b001),
    'hamming' / BitsInteger(3),
    'F' / Flag, # type: ignore
    'common_mode0' / BitsInteger(10),
    'common_mode1' / BitsInteger(10),
    'channel_map' / If(this.F == False, ChannelMap(BitsInteger(37))),
    'E' / If(this.F == True, Flag),
    'padding' / If(this.F == True, Const(0, BitsInteger(4))),
)

eRxChannelDataPassThrough = BitStruct(
    'TcTp' / FlagsEnum(BitsInteger(2), Tc=0b10, Tp=0b01),
    'roc_field1' / BitsInteger(10),
    'roc_field2' / BitsInteger(10),
    'roc_field3' / BitsInteger(10),
)

eRxChannelPassThrough = Struct(
    'subpacket_header' / SubpacketHeader,
    'channel_data' / If(this.subpacket_header.F == False,
                        Array(37, eRxChannelDataPassThrough)),
)

eRxChannelDataStandard = BitStruct(
    'code1' / BitsInteger(2),
    'code2' / If(this.code1 == 0b00, BitsInteger(2)),
    'ADC-1' / If(this.code1 != 0b00 or this.code2 & 0b0 == 0b0, BitsInteger(10)),
    'ADC/TOT' / BitsInteger(10),
    'TOA' / If(this.code1 != 0b00 or this.code2 == 0b11, BitsInteger(10)),
    'Padding' / If(this.code1 == 0b00 and this.code2 == 0b01, Const(0, BitsInteger(2))),
)

eRxCannelStandard = Struct(
    'subpacket_header' / SubpacketHeader,
    'channel_data' / If(this.subpacket_header.F == 0, 
                        RawCopy(Array(len_(this.subpacket_header.channel_map), eRxChannelDataStandard))),
    'padding' / If(this.subpacket_header.F == 0, Const(0, Bitwise(BitsInteger((this.channel_data.length % 4) * 8)))),
)

ECONDIdle = BitStruct(
    'pattern' / Const(0xaaaaff, BitsInteger(24)),
    'reset_request' / BitsInteger(2),
    'errors' / FlagsEnum(BitsInteger(3), error_econd=1, error_econt=2, error_rocs=4),
    'buffer_state' / BitsInteger(3),
)

EconDPacket = Struct(
    'header' / Header,
    'payload' / If(this.header.header_body.value.truncated == False,
                   RawCopy(
                       IfThenElse(
                            this.header.header_body.value.pass_through == 1,
                            Array(ECON_ERX, eRxChannelPassThrough),
                            Array(ECON_ERX, eRxCannelStandard)))),

    'crc' / If(this.header.header_body.value.truncated == False,
               Checksum(Bytes(4),
                    lambda data: calculate_econd_payload_crc(data),
                    this.payload.data)),
    'mandatory_idle' / ECONDIdle,
)
