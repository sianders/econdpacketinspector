import linecache
from bitstring import BitArray
import pandas as pd
import econd_packages
from construct.lib.containers import Container, ListContainer


def get_columns(filename: str, skip_rows: int) -> int:
    line = linecache.getline(filename, skip_rows+1)
    cols = line.split()
    ncols = len(cols)
    print(f'Detected {ncols} columns')
    return ncols

def get_serenity_etx_dataframe(filename: str, skiprows: int, columns: int) -> pd.DataFrame:
    convert_dict = dict()
    last_n_chars = -8
    for row_index in range(2, columns):
        if row_index % 2 == 0:
            convert_dict[row_index] = lambda string: BitArray(bin=string)
        else:
            convert_dict[row_index] = lambda string: BitArray(hex=string[last_n_chars:])


    df = pd.read_csv(filename, header=None, skiprows=skiprows, delim_whitespace=True, index_col=1, converters=convert_dict)
    return df

def get_zcu_link_capture_dataframe(filename) -> pd.DataFrame:
    convert_dict = {0: lambda string: BitArray(hex=string), 1: lambda string: BitArray(hex=string)}
    df = pd.read_csv(filename, header=None, converters=convert_dict)
    return df 

def get_fiber_dataframe(df: pd.DataFrame, fiber: int) -> pd.DataFrame:
    daq_cols = [fiber * 2 + 2, fiber * 2 + 3]
    fiber_df = df[daq_cols]

    # Find the first row where column 0 is 0 and truncate the dataframe
    for row_index, row in fiber_df.iterrows():
        if row[daq_cols[0]].bin == "00000":
            fiber_df = fiber_df.iloc[row_index:]
            break
    
    # Keep only the fiber column
    fiber_df = fiber_df[[daq_cols[1]]]

    # Reset indexes of dataframe
    fiber_df.columns = range(fiber_df.columns.size) # type: ignore
    fiber_df.reset_index(drop=True, inplace=True)

    return fiber_df # type: ignore

def convert_to_elink_dataframe(df: pd.DataFrame) -> pd.DataFrame:
    # Convert into eLink columns, where each row is a 32-bit word and every 8th row is an eLink (Excluding delimiter)
    df_len = (len(df)//8) * 8
    df = df[:df_len] # Round to nearst 8
    index = pd.MultiIndex.from_product([range(df_len//8), range(-1,7)]) # Make MultiIndex for unstack
    df.index = index
    elink_df = df.unstack(level=1).droplevel(0, axis=1).loc[:, 0:]

    return elink_df

def convert_to_econd_payload_dataframe(elink_df: pd.DataFrame, pairs: dict) -> pd.DataFrame:
    # Get full links data
    payloads_df = pd.DataFrame(columns=[k for k in pairs])
    for key, pair in pairs.items():
        payloads_df[key] = elink_df.loc[:, pair].stack().reset_index(drop=True)


    return payloads_df

def convert_to_bitarrays(payloads_df: pd.DataFrame) -> dict:
    bitarray_payloads = dict()

    for column in payloads_df.columns:
        bitarray_payloads.update({column: BitArray().join(payloads_df[column])})
     
    return bitarray_payloads

def get_header_index(bitarray_payloads: BitArray) -> int:
    header_index = None

    def try_parse(x) -> bool:
        try:
            econd_packages.Header.parse(x.bytes)
            return True
        except:
            return False
    
    n = 64
    ## Slow but generator - Might be able to get optimized
    # header_matches = (f for f in (bitarray_payloads[x:x+n] for x in range(0, len(bitarray_payloads), n)) if try_parse(f))
    # good_header = next(header_matches, None)
    # if good_header is None:
    #     raise ValueError("No header found")
    
    # header_index = bitarray_payloads.find(good_header)[0]
    
    # Faster but for loop
    for head in range(len(bitarray_payloads)//n):
        if try_parse(bitarray_payloads[head*n:(head+1)*n]):
            header_index = head * n
            break
    if header_index is None:
        raise ValueError("No header found")

    return header_index

non_fields = ["_io"]

def remove_rawcopy(parsed):
    try:
        parsed = parsed.value
    except:
        pass
    
    if isinstance(parsed, Container):
        container = Container()

        for field in parsed:
            if (field not in non_fields and isinstance(field, str)):
                x = getattr(parsed, field)
                if not isinstance(x, (Container, ListContainer)):
                    container.update({field: x})    
                    continue
                
                container.update({field: remove_rawcopy(x)})

        return container
    elif isinstance(parsed, ListContainer):
        container = ListContainer()

        for field in parsed:
            container.append(remove_rawcopy(field))

        return container
    
    raise("The parsed value is of type", type(parsed), "which is not accepted")

    
    
            
