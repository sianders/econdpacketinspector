
from crc import CrcCalculator, Configuration
from bitstring import BitArray

econd_header_crc_config = Configuration(
    width=8,
    polynomial=0xa7,
    init_value=0x00,
    final_xor_value=0x00,
    reverse_input=False,
    reverse_output=False,
)

econd_payload_crc_config = Configuration(
    width=32,
    polynomial=0x04c11db7,
    init_value=0x00000000,
    final_xor_value=0x00000000,
    reverse_input=False,
    reverse_output=False,
)

econd_header_calculator = CrcCalculator(econd_header_crc_config)
econd_payload_calculator = CrcCalculator(econd_payload_crc_config)

def calculate_econd_header_crc(header: bytes) -> bytes:
    hamming_shift = 32 - 6 # Shift from the left
    hamming_mask = '0b000000'

    header_copy = BitArray(header)
    header_copy.overwrite(hamming_mask, hamming_shift)
    
    crc = econd_header_calculator.calculate_checksum(header_copy.bytes)

    return crc.to_bytes(1, byteorder='big')

def calculate_econd_payload_crc(payload: bytes) -> bytes:
    crc = econd_payload_calculator.calculate_checksum(payload)
    return crc.to_bytes(4, byteorder='big')