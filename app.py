from time import time
import click
import package_decoder_utils as utils
from bitstring import BitArray
import econd_packages
from construct import *

@click.command()
@click.version_option()
@click.argument("type", type=click.Choice(["serenity_etx", "zcu_link_capture"]))
@click.argument("filename", type=click.Path(exists=True))

def main(type: str, filename: str) -> None:
    if (type == "serenity_etx"):
        decode_serenity_etx(filename)
    elif (type == "zcu_link_capture"):
        decode_zcu_link_capture(filename)

def decode_zcu_link_capture(filename: str) -> None:
    start_time = time()

    found_col_time = time()

    elink_df = utils.get_zcu_link_capture_dataframe(filename)

    read_csv_time = time()

    # Get full links data
    pairs = {"a": [1, 0]}
    payloads_df = utils.convert_to_econd_payload_dataframe(elink_df, pairs)

    make_payloads_time = time()

    # Convert to bitarrays
    bitarray_payloads = utils.convert_to_bitarrays(payloads_df)

    make_bytearray_time = time()

    # long_idle
    # long_idle = BitArray().join([BitArray("0xaaaaff080xaaaaff08") for x in range(2000)])
    # bitarray_payloads[0] = long_idle + bitarray_payloads[0]

    find_header_begin_tiem = time()

    header_index = utils.get_header_index(bitarray_payloads["a"])
    find_header_end_time = time()
    
    parsed = econd_packages.EconDPacket.parse(bitarray_payloads["a"][header_index:].bytes)

    parsed_time = time()

    parsed = utils.remove_rawcopy(parsed)
    
    remove_raw_copy_time = time()

    print(parsed)

    print("Find col time", found_col_time - start_time)
    print("Read csv time", read_csv_time - found_col_time)
    print("Make payloads time", make_payloads_time - read_csv_time)
    print("Make bytearrays time", make_bytearray_time - make_payloads_time)
    print("Find header time", find_header_end_time - find_header_begin_tiem)
    print("Parse time", parsed_time - find_header_end_time)
    print("Remove raw copy time", remove_raw_copy_time - parsed_time)

def decode_serenity_etx(filename: str) -> None:
    start_time = time()
    skiprows = 4

    ncols = utils.get_columns(filename, skiprows)

    found_col_time = time()

    df = utils.get_serenity_etx_dataframe(filename, skiprows, ncols)

    read_csv_time = time()

    # Get columns of DAQ data
    daq_nfiber = 2
    
    daq_df = utils.get_fiber_dataframe(df, daq_nfiber)

    truncate_df_time = time()

    # Convert into eLink columns, where each row is a 32-bit word and every 8th row is an eLink (Excluding delimiter)
    elink_df = utils.convert_to_elink_dataframe(daq_df)

    convert_to_elink_time = time()


    # Get full links data
    pairs = {"a": [5, 4]}
    payloads_df = utils.convert_to_econd_payload_dataframe(elink_df, pairs)

    make_payloads_time = time()

    # Convert to bitarrays
    bitarray_payloads = utils.convert_to_bitarrays(payloads_df)

    make_bytearray_time = time()

    # long_idle
    # long_idle = BitArray().join([BitArray("0xaaaaff080xaaaaff08") for x in range(2000)])
    # bitarray_payloads[0] = long_idle + bitarray_payloads[0]

    find_header_begin_tiem = time()

    header_index = utils.get_header_index(bitarray_payloads['a'])

    find_header_end_time = time()

    parsed = econd_packages.EconDPacket.parse(bitarray_payloads['a'][header_index:].bytes)

    parsed_time = time()

    parsed = utils.remove_rawcopy(parsed)

    remove_raw_copy_time = time()

    print(parsed)

    print("Find col time", found_col_time - start_time)
    print("Read csv time", read_csv_time - found_col_time)
    print("Get DAQ df time", truncate_df_time - read_csv_time)
    print("Convert to elink time", convert_to_elink_time - truncate_df_time)
    print("Make payloads time", make_payloads_time - convert_to_elink_time)
    print("Make bytearrays time", make_bytearray_time - make_payloads_time)
    print("Find header time", find_header_end_time - find_header_begin_tiem)
    print("Parse time", parsed_time - find_header_end_time)
    print("Remove raw copy time", remove_raw_copy_time - parsed_time)




    
if __name__ == "__main__":
    main()

